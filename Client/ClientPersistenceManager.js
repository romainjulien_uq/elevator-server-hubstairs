"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Mongo_1 = require("../Mongo/Mongo");
/**
 * Cette classe est utilisé pour insérer/query dans la base de donnée mongo.
 */
var ClientPersistenceManager = (function () {
    function ClientPersistenceManager() {
    }
    /**
     * Methode appelée lorsque l'ascenseur a été appelée, et lorsqu'il s'arrete.
     * @param log - un document dans la collection
     * @param collection - Le nom de la collection est la date du jour au format 'dd-mm-yyyy'
     */
    ClientPersistenceManager.prototype.insertActivityLog = function (log, collection) {
        Mongo_1.MongoDB.db.collection(collection).insertOne(log).catch(function (err) {
            console.log("error inserting " + log + " in mongoDB:");
            console.log(err);
        });
    };
    /**
     * Methode appelée lorque la page admin le demande.
     * @param collection - Le nom de la collection est la date du jour au format 'dd-mm-yyyy'
     * @returns {T[]} Retourne tous les documents de la collection
     */
    ClientPersistenceManager.prototype.getActivityLogs = function (collection) {
        return Mongo_1.MongoDB.db.collection(collection).find().toArray();
    };
    return ClientPersistenceManager;
}());
exports.ClientPersistenceManager = ClientPersistenceManager;
