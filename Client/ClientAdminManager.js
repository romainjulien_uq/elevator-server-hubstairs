"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Classe responsable de la page admin.
 */
var ClientAdminManager = (function () {
    function ClientAdminManager(communicationManager, persistenceManager) {
        this.communicationManager = communicationManager;
        this.persistenceManager = persistenceManager;
    }
    /**
     * Methode appelée lorsque le client requiert l'activité de l'ascenseur à un jour donné
     * @param data - Interface: msgEntrant.RequestActivityLog
     */
    ClientAdminManager.prototype.handleGetActivityLog = function (data) {
        var _this = this;
        this.persistenceManager.getActivityLogs(data.collection).then(function (r) { return _this.replyToClient(r); });
    };
    /**
     * Envois la reponse au client.
     * @param r Le resultat de la query sur mongo
     */
    ClientAdminManager.prototype.replyToClient = function (r) {
        // l'idée etait de renforcer le format des documents tel que defini par l'interface
        var response = r;
        var toSend = { allLogs: response };
        this.communicationManager.sendMessage('response activity log', toSend);
    };
    return ClientAdminManager;
}());
exports.ClientAdminManager = ClientAdminManager;
