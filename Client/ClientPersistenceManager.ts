import {MongoDB} from "../Mongo/Mongo";
import {ActivityLog} from "../Mongo/ActivityLogIF";

/**
 * Cette classe est utilisé pour insérer/query dans la base de donnée mongo.
 */
export class ClientPersistenceManager {

    constructor() {}

    /**
     * Methode appelée lorsque l'ascenseur a été appelée, et lorsqu'il s'arrete.
     * @param log - un document dans la collection
     * @param collection - Le nom de la collection est la date du jour au format 'dd-mm-yyyy'
     */
    public insertActivityLog(log: ActivityLog, collection: String) {

        MongoDB.db.collection(collection).insertOne(
            log
        ).catch(function(err) {
            console.log("error inserting " + log + " in mongoDB:");
            console.log(err);
        });
    }

    /**
     * Methode appelée lorque la page admin le demande.
     * @param collection - Le nom de la collection est la date du jour au format 'dd-mm-yyyy'
     * @returns {T[]} Retourne tous les documents de la collection
     */
    public getActivityLogs(collection: String) {
        return MongoDB.db.collection(collection).find().toArray();
    }
}