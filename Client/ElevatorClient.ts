import {ClientCommunicationManager} from "./ClientCommunicationManager";
import {ClientPersistenceManager} from "./ClientPersistenceManager";
import {ClientAdminManager} from "./ClientAdminManager";
import {ActivityLog} from '../Mongo/ActivityLogIF'
import {ActivityEnum} from '../Mongo/ActivityEnum'
import * as msgEntrant from '../Messagerie/MessagesEntrantsIF'
import * as msgSortant from '../Messagerie/MessagesSortantsIF'

/**
 * A chaque nouvelle connection, un ElevatorClient est instancié.
 * Chaque Elevator client 'encapsule' son propre ClientCommunicationManager, ClientPersistenceManager, et
 * ClientAdminManager.
 */
export class ElevatorClient {

    private communicationManager: ClientCommunicationManager;
    private persistenceManager: ClientPersistenceManager;
    private adminManager: ClientAdminManager;

    // Indique si l'ascenseur est en mouvement
    private isMoving: boolean;
    // Indique si l'ascenseur monte
    private isGoingUp: boolean;
    // Indique a quel etage est l'ascenseur
    private currentFloor: number;
    // Une liste des etages qui ont été appelé (dans l'ordre), le 1er element est le prochain
    // etage où l'ascenseur doit aller, le dernier element correspond au dernier bouton préssé TODO optimiser
    private floorsToReach: Array<number>;
    // Un interval node, utilisé pour attendre une seconde entre chaque changement d'etage
    private floorInterval: any;

    constructor(clientSocket: any) {
        this.communicationManager = new ClientCommunicationManager(this, clientSocket);
        this.persistenceManager = new ClientPersistenceManager();
        this.adminManager = new ClientAdminManager(this.communicationManager, this.persistenceManager);

        this.isMoving = false;
        this.isGoingUp = false;
        this.currentFloor = 3;
        this.floorsToReach = new Array();

        // Dis au client de commencer à l'etage 3
        let toSend: msgSortant.ElevatorAtFloor = {floor: this.currentFloor};
        this.communicationManager.sendMessage('elevator at floor', toSend);
    }

    /**
     * Methode appelée lorsque l'ascenseur est appelé à un etage (bouton préssé client-side)
     * @param data - l'etage où l'ascenseur doit aller
     */
    public handleElevatorCalled(data: msgEntrant.ElevatorCalled) {
        // Ajoute l'etage a la fin de la liste des etages ou l'ascenseur doit aller
        // TODO optimise: si un etage est appelé il ne doit pas necessairement aller a la fin de la liste
        this.floorsToReach.push(data.floor);
        if (!this.isMoving) {
            this.startElevator();
        }

        this.storeLogInDatabase(ActivityEnum.CALLED, data.floor);
    }

    /**
     * Getter pour le ClientCommunicationManager
     * @returns {ClientAdminManager}
     */
    public getAdminManager(): ClientAdminManager {
        return this.adminManager;
    }

    /**
     * Demarre l'ascenseur si this.floorsToReach contient au moins un etage ou aller, et si l'ascenseur n'est
     * pas deja a l'etage ou il doit aller.
     */
    private startElevator() {
        if (this.floorsToReach.length == 0) {
            this.isMoving = false;
            return;
        }

        // Recuperer le premier element de la liste - l'etage ou l'ascenseur doit maintenant se rendre
        let floorToGo = this.floorsToReach.shift();
        if (floorToGo == this.currentFloor) {
            this.elevatorStopped();
        } else {
            this.elevatorWillMove(floorToGo);
        }
    }

    /**
     * L'ascenseur etait arrété et s'apprete a redemarrer,
     * @param floorToGo
     */
    private elevatorWillMove(floorToGo: number) {
        this.isGoingUp = floorToGo > this.currentFloor;
        this.floorInterval = setInterval(() => this.elevatorHasMoved(floorToGo), 1000);
        this.isMoving = true;

        let toSend: msgSortant.ElevatorStart = {isGoingUp: this.isGoingUp};
        this.communicationManager.sendMessage('elevator start', toSend);
    }

    /**
     * L'ascenseur etait en mouvement et a atteint un nouvel etage,
     * si c'etait l'etage a atteindre on arrete l'interval et l'ascenseur s'arrete
     * @param floorToGo
     */
    private elevatorHasMoved(floorToGo: number) {
        this.currentFloor += (this.isGoingUp ? 1 : -1);
        if (this.currentFloor == floorToGo) {
            this.elevatorStopped();
            clearInterval(this.floorInterval);
        }

        let toSend: msgSortant.ElevatorAtFloor = {floor: this.currentFloor};
        this.communicationManager.sendMessage('elevator at floor', toSend);
    }

    /**
     * Methode appelé lorsque l'ascenseur s'arrete.
     * On previent le client et on attend 5 secondes
     */
    private elevatorStopped() {
        setTimeout(() => this.startElevator(), 5000);
        let toSend: msgSortant.ElevatorStop = {floor: this.currentFloor};
        this.communicationManager.sendMessage('elevator stop', toSend);

        this.storeLogInDatabase(ActivityEnum.ARRIVED, this.currentFloor);
    }

    /**
     * Methode appelé lorsque l'ascenseur est appelé a un etage et lorsqu'il s'arrete a un etage.
     * @param activity - CALLED ou ARRIVED
     * @param floor - L'etage ou l'ascenseur doit aller / L'etage ou l'ascenseur s'est arrété
     */
    private storeLogInDatabase(activity: ActivityEnum, floor: number) {
        let timestamp = new Date();
        let collection = timestamp.getDay() + "-" + timestamp.getMonth() + "-" + timestamp.getFullYear();
        let log: ActivityLog = {
            activity: ActivityEnum[activity],
            floor: floor,
            timestamp: timestamp
        };

        this.persistenceManager.insertActivityLog(log, collection);
    }
}