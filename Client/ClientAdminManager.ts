import {ClientCommunicationManager} from "./ClientCommunicationManager";
import {ClientPersistenceManager} from "./ClientPersistenceManager";
import * as msgEntrant from '../Messagerie/MessagesEntrantsIF'
import * as msgSortant from '../Messagerie/MessagesSortantsIF'
import {ActivityLog} from "../Mongo/ActivityLogIF";

/**
 * Classe responsable de la page admin.
 */
export class ClientAdminManager {

    private communicationManager: ClientCommunicationManager;
    private persistenceManager: ClientPersistenceManager;

    constructor(communicationManager: ClientCommunicationManager, persistenceManager: ClientPersistenceManager) {
        this.communicationManager = communicationManager;
        this.persistenceManager = persistenceManager;
    }

    /**
     * Methode appelée lorsque le client requiert l'activité de l'ascenseur à un jour donné
     * @param data - Interface: msgEntrant.RequestActivityLog
     */
    public handleGetActivityLog(data: msgEntrant.RequestActivityLog) {
        this.persistenceManager.getActivityLogs(data.collection).then((r) => this.replyToClient(r));
    }

    /**
     * Envois la reponse au client.
     * @param r Le resultat de la query sur mongo
     */
    private replyToClient(r: any) {
        // l'idée etait de renforcer le format des documents tel que defini par l'interface
        let response: ActivityLog[] = r;

        let toSend: msgSortant.ResponseActivityLog = {allLogs: response};
        this.communicationManager.sendMessage('response activity log', toSend);
    }

}