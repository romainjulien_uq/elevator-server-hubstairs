import {ElevatorClient} from "./ElevatorClient";
import * as msgSortant from '../Messagerie/MessagesSortantsIF'

/**
 * Cette classe s'occupe de toute la communication (entrante/sortante) avec le client.
 * La methode sendMessage() est appelée pour chaque message devant etre envoyé au serveur.
 * Chaque message recu provoque l'appel de la methode messageReceived()
 *
 * Note - Penser a ajouter les listeners pour chaque message entrant
 */
export class ClientCommunicationManager {

    private elevatorClient: ElevatorClient;
    private clientSocket: any;

    constructor(elevatorClient: ElevatorClient, clientSocket: any) {
        this.elevatorClient = elevatorClient;
        this.clientSocket = clientSocket;

        this.addListeners();
    }

    /**
     * Le point de sortie de tous les message vers le client.
     * @param event
     * @param message
     */
    public sendMessage(event: String, message: msgSortant.MsgSortant) {
        this.clientSocket.emit(event, message);
    }

    /**
     * Chaque message entrant requiert d'ajouter un listener
     */
    private addListeners() {
        this.clientSocket.on('elevator called', (data) => this.messageReceived('elevator called', data));
        this.clientSocket.on('request activity log', (data) => this.messageReceived('request activity log', data));
    }

    /**
     * Le point d'entrée de tous les message du client.
     * @param event
     * @param message
     */
    private messageReceived(eventName: String, data: any) {
        switch (eventName) {
            case 'elevator called':
                this.elevatorClient.handleElevatorCalled(data);
                break;
            case 'request activity log':
                this.elevatorClient.getAdminManager().handleGetActivityLog(data);
                break;
            default:
                break;
        }
    }
}