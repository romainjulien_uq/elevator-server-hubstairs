"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ClientCommunicationManager_1 = require("./ClientCommunicationManager");
var ClientPersistenceManager_1 = require("./ClientPersistenceManager");
var ClientAdminManager_1 = require("./ClientAdminManager");
var ActivityEnum_1 = require("../Mongo/ActivityEnum");
/**
 * A chaque nouvelle connection, un ElevatorClient est instancié.
 * Chaque Elevator client 'encapsule' son propre ClientCommunicationManager, ClientPersistenceManager, et
 * ClientAdminManager.
 */
var ElevatorClient = (function () {
    function ElevatorClient(clientSocket) {
        this.communicationManager = new ClientCommunicationManager_1.ClientCommunicationManager(this, clientSocket);
        this.persistenceManager = new ClientPersistenceManager_1.ClientPersistenceManager();
        this.adminManager = new ClientAdminManager_1.ClientAdminManager(this.communicationManager, this.persistenceManager);
        this.isMoving = false;
        this.isGoingUp = false;
        this.currentFloor = 3;
        this.floorsToReach = new Array();
        // Dis au client de commencer à l'etage 3
        var toSend = { floor: this.currentFloor };
        this.communicationManager.sendMessage('elevator at floor', toSend);
    }
    /**
     * Methode appelée lorsque l'ascenseur est appelé à un etage (bouton préssé client-side)
     * @param data - l'etage où l'ascenseur doit aller
     */
    ElevatorClient.prototype.handleElevatorCalled = function (data) {
        // Ajoute l'etage a la fin de la liste des etages ou l'ascenseur doit aller
        // TODO optimise: si un etage est appelé il ne doit pas necessairement aller a la fin de la liste
        this.floorsToReach.push(data.floor);
        if (!this.isMoving) {
            this.startElevator();
        }
        this.storeLogInDatabase(ActivityEnum_1.ActivityEnum.CALLED, data.floor);
    };
    /**
     * Getter pour le ClientCommunicationManager
     * @returns {ClientAdminManager}
     */
    ElevatorClient.prototype.getAdminManager = function () {
        return this.adminManager;
    };
    /**
     * Demarre l'ascenseur si this.floorsToReach contient au moins un etage ou aller, et si l'ascenseur n'est
     * pas deja a l'etage ou il doit aller.
     */
    ElevatorClient.prototype.startElevator = function () {
        if (this.floorsToReach.length == 0) {
            this.isMoving = false;
            return;
        }
        // Recuperer le premier element de la liste - l'etage ou l'ascenseur doit maintenant se rendre
        var floorToGo = this.floorsToReach.shift();
        if (floorToGo == this.currentFloor) {
            this.elevatorStopped();
        }
        else {
            this.elevatorWillMove(floorToGo);
        }
    };
    /**
     * L'ascenseur etait arrété et s'apprete a redemarrer,
     * @param floorToGo
     */
    ElevatorClient.prototype.elevatorWillMove = function (floorToGo) {
        var _this = this;
        this.isGoingUp = floorToGo > this.currentFloor;
        this.floorInterval = setInterval(function () { return _this.elevatorHasMoved(floorToGo); }, 1000);
        this.isMoving = true;
        var toSend = { isGoingUp: this.isGoingUp };
        this.communicationManager.sendMessage('elevator start', toSend);
    };
    /**
     * L'ascenseur etait en mouvement et a atteint un nouvel etage,
     * si c'etait l'etage a atteindre on arrete l'interval et l'ascenseur s'arrete
     * @param floorToGo
     */
    ElevatorClient.prototype.elevatorHasMoved = function (floorToGo) {
        this.currentFloor += (this.isGoingUp ? 1 : -1);
        if (this.currentFloor == floorToGo) {
            this.elevatorStopped();
            clearInterval(this.floorInterval);
        }
        var toSend = { floor: this.currentFloor };
        this.communicationManager.sendMessage('elevator at floor', toSend);
    };
    /**
     * Methode appelé lorsque l'ascenseur s'arrete.
     * On previent le client et on attend 5 secondes
     */
    ElevatorClient.prototype.elevatorStopped = function () {
        var _this = this;
        setTimeout(function () { return _this.startElevator(); }, 5000);
        var toSend = { floor: this.currentFloor };
        this.communicationManager.sendMessage('elevator stop', toSend);
        this.storeLogInDatabase(ActivityEnum_1.ActivityEnum.ARRIVED, this.currentFloor);
    };
    /**
     * Methode appelé lorsque l'ascenseur est appelé a un etage et lorsqu'il s'arrete a un etage.
     * @param activity - CALLED ou ARRIVED
     * @param floor - L'etage ou l'ascenseur doit aller / L'etage ou l'ascenseur s'est arrété
     */
    ElevatorClient.prototype.storeLogInDatabase = function (activity, floor) {
        var timestamp = new Date();
        var collection = timestamp.getDay() + "-" + timestamp.getMonth() + "-" + timestamp.getFullYear();
        var log = {
            activity: ActivityEnum_1.ActivityEnum[activity],
            floor: floor,
            timestamp: timestamp
        };
        this.persistenceManager.insertActivityLog(log, collection);
    };
    return ElevatorClient;
}());
exports.ElevatorClient = ElevatorClient;
