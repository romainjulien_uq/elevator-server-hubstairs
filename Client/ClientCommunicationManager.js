"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Cette classe s'occupe de toute la communication (entrante/sortante) avec le client.
 * La methode sendMessage() est appelée pour chaque message devant etre envoyé au serveur.
 * Chaque message recu provoque l'appel de la methode messageReceived()
 *
 * Note - Penser a ajouter les listeners pour chaque message entrant
 */
var ClientCommunicationManager = (function () {
    function ClientCommunicationManager(elevatorClient, clientSocket) {
        this.elevatorClient = elevatorClient;
        this.clientSocket = clientSocket;
        this.addListeners();
    }
    /**
     * Le point de sortie de tous les message vers le client.
     * @param event
     * @param message
     */
    ClientCommunicationManager.prototype.sendMessage = function (event, message) {
        this.clientSocket.emit(event, message);
    };
    /**
     * Chaque message entrant requiert d'ajouter un listener
     */
    ClientCommunicationManager.prototype.addListeners = function () {
        var _this = this;
        this.clientSocket.on('elevator called', function (data) { return _this.messageReceived('elevator called', data); });
        this.clientSocket.on('request activity log', function (data) { return _this.messageReceived('request activity log', data); });
    };
    /**
     * Le point d'entrée de tous les message du client.
     * @param event
     * @param message
     */
    ClientCommunicationManager.prototype.messageReceived = function (eventName, data) {
        switch (eventName) {
            case 'elevator called':
                this.elevatorClient.handleElevatorCalled(data);
                break;
            case 'request activity log':
                this.elevatorClient.getAdminManager().handleGetActivityLog(data);
                break;
            default:
                break;
        }
    };
    return ClientCommunicationManager;
}());
exports.ClientCommunicationManager = ClientCommunicationManager;
