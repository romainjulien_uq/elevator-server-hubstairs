import {MongoDB} from "./Mongo/Mongo";
import io = require('socket.io');
import {ElevatorClient} from './Client/ElevatorClient';

/**
 * Le point d'entrée de l'application, cette classe initialize la socket serveur, et cree un
 * nouveau ElevatorClient a chaque connection.
 */
class ElevatorServer {

    constructor(port: number) {
        let socket = io();
        socket.on('connection', (client) => this.createNewClient(client));
        socket.listen(port);
    }

    private createNewClient(client: any) {
        console.log("client connecté");
        new ElevatorClient(client);
    }

}

new ElevatorServer(3001);
new MongoDB('localhost', '27017', 'elevatordb');