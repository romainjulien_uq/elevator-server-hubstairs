"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Mongo_1 = require("./Mongo/Mongo");
var io = require("socket.io");
var ElevatorClient_1 = require("./Client/ElevatorClient");
/**
 * Le point d'entrée de l'application, cette classe initialize la socket serveur, et cree un
 * nouveau ElevatorClient a chaque connection.
 */
var ElevatorServer = (function () {
    function ElevatorServer(port) {
        var _this = this;
        var socket = io();
        socket.on('connection', function (client) { return _this.createNewClient(client); });
        socket.listen(port);
    }
    ElevatorServer.prototype.createNewClient = function (client) {
        console.log("client connecté");
        new ElevatorClient_1.ElevatorClient(client);
    };
    return ElevatorServer;
}());
new ElevatorServer(3001);
new Mongo_1.MongoDB('localhost', '27017', 'elevatordb');
