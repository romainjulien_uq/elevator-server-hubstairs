/**
 * Format des messages envoyés par le client
 */

/**
 * Lorsque l'ascenseur est appelé à un etage
 * floor: l'etage où l'ascenseur doit se rendre
 */
export interface ElevatorCalled {
    floor: number
}

/**
 * Lorsque le client (page admin) demande la liste des activités de l'ascenseur
 * collection: la collection requise - la date du jour
 */
export interface RequestActivityLog {
    collection: String
}