/**
 * Format des messages envoyés par le serveur
 */

import {ActivityLog} from "../Mongo/ActivityLogIF";

/**
 * Un container pour tous les messages sortants
 */
export interface MsgSortant {}

/**
 * Message envoyé lorsque l'ascenseur demarre
 */
export interface ElevatorStart extends MsgSortant{
    isGoingUp: boolean
}

/**
 * Message envoyé lorsque l'ascenseur s'arrete
 */
export interface ElevatorStop extends MsgSortant{
    floor: number
}

/**
 * Message enoyé lorsque l'ascenseur atteint un etage (en mouvement ou immobilisé)
 */
export interface ElevatorAtFloor extends MsgSortant{
    floor: number
}

/**
 * Une reponse envoyé suite a une demande de la liste des activités de l'ascenseur
 */
export interface ResponseActivityLog extends MsgSortant {
    allLogs: ActivityLog[];
}