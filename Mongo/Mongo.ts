import MongoClient = require('mongodb');

/**
 * Initialise la connection avec la base de donnée.
 */
export class MongoDB {

    static db: any;

    constructor(ip: String, port: String, dbName: String) {

        let url = 'mongodb://' + ip + ':' + port + '/' + dbName;

        MongoClient.connect(url, function (err, database) {
            if (err == null) {
                MongoDB.db = database;
                console.log("Connected to Mongo");
            } else {
                console.log("Error connecting to Mongo - " + err);
            }
            //TODO s'occuper de MongoDB.db.close();
        });
    }

}

