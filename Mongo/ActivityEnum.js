"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Deux types d'activités sont enregistrées dans la base de donnée:
 * - CALLED lorsque l'ascenseur est appelé pour aller a un etage (panneau ou immeuble)
 * - ARRIVED lorsque l'ascensseur est arrivé à cet etage
 */
var ActivityEnum;
(function (ActivityEnum) {
    ActivityEnum[ActivityEnum["CALLED"] = 0] = "CALLED";
    ActivityEnum[ActivityEnum["ARRIVED"] = 1] = "ARRIVED";
})(ActivityEnum = exports.ActivityEnum || (exports.ActivityEnum = {}));
