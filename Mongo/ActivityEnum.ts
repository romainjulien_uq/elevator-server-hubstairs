/**
 * Deux types d'activités sont enregistrées dans la base de donnée:
 * - CALLED lorsque l'ascenseur est appelé pour aller a un etage (panneau ou immeuble)
 * - ARRIVED lorsque l'ascensseur est arrivé à cet etage
 */
export enum ActivityEnum {
    CALLED ,
    ARRIVED
}
