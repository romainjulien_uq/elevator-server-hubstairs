"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MongoClient = require("mongodb");
/**
 * Initialise la connection avec la base de donnée.
 */
var MongoDB = (function () {
    function MongoDB(ip, port, dbName) {
        var url = 'mongodb://' + ip + ':' + port + '/' + dbName;
        MongoClient.connect(url, function (err, database) {
            if (err == null) {
                MongoDB.db = database;
                console.log("Connected to Mongo");
            }
            else {
                console.log("Error connecting to Mongo - " + err);
            }
            //TODO s'occuper de MongoDB.db.close();
        });
    }
    return MongoDB;
}());
exports.MongoDB = MongoDB;
