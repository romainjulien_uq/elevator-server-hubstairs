/**
 * U document - une entrée - dans une collection mongo
 * activity: ActivityEnum -> representation String
 * floor: l'etage
 * timestamp: la date et l'heure de l'activité
 */
export interface ActivityLog {
    activity: String,
    floor: number,
    timestamp: Date
}